const { Pool } = require('pg')

class Db {
    
    constructor() {
        this.config = {
            user: 'postgres',
            host: 'localhost',
            database: 'planets',
            password: 'Pringles2019',
            port: 5432
        };
        this.pool = new Pool(this.config);
    }

    getPool() {
        return this.pool;
    }
    
    getPlanets(req, resp) {
        this.pool.query('SELECT * FROM planets', (err, results) => {
            if(err) {
                resp.status(500).json({
                    success: false,
                    message: err
                });
                resp.end();
            } 
            resp.status(200).json(results.rows);   
        });
    }

    async addPlanet(req, resp) {
        try {
            const {name, size, date_discovered} = req.body;
            const results = await this.pool.query('INSERT INTO planets (name, size, date_discovered) VALUES ($1, $2, $3)', [name, size, date_discovered]);
            resp.json(results);
        }
        catch(e) {
        } 
    }

    deletePlanet(req, resp) {
        this.pool.query('DELETE FROM planets WHERE id = $1', [req.body.id], (err, result) => {
            resp.json(result);
        });
        //resp.json();
    }

    async updatePlanet(req, resp) {
        try {
            const {id, name, size, date_discovered} = req.body;
            const result = await this.pool.query('UPDATE planets SET name = $1, size = $2, date_discovered = $3 WHERE id = $4', [name, size, date_discovered, id]);
            resp.json(result);
        } catch (error) {
            console.log(error);
        }
    }
}

module.exports.db = new Db();
/*



updatePlanet(req, resp) {

}
*/