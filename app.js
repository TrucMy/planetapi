const { Pool } = require('pg');
const express = require('express');

const app = express();

const { db } = require('./db.js')
const { Auth } = require('./auth');

const publicPath = [
    '/api/v1/auth/generate-key',
    '/api/v1/planets/getAllPlanets'
];

//const planet = new planet(db.getPool());
const auth = new Auth(db.getPool());

app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

app.use(async (req, resp, next) => {
    if (publicPath.includes(req.originalUrl)) {
        return next();
    }

    const authResult = await auth.authenticateKey(req, resp);

    if (authResult === true) {
        next();
    } else {
        resp.status(401).json(authResult);
    }
});

app.get('/api/v1/planets', (request, response) => db.getPlanets(request, response));
app.post('/api/v1/planets/add', (request, response) => db.addPlanet(request, response));
app.delete('/api/v1/planets/delete', (request, response) => db.deletePlanet(request, response));
app.put('/api/v1/planets/update', (request, response) => db.updatePlanet(request, response));

//Auth
app.post('/api/v1/auth/generate-key', (req, resp) => auth.generateKey(req, resp));

app.listen(3434, () => {
    console.log('Listening');
});