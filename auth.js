const crypto = require('crypto');

class Auth {
    constructor(pool) {
        this.pool = pool;
    }

    async authenticateKey(req, resp) {
        try {
            const { authorization } = req.headers;

            if (!authorization) {
                return {
                    error: 'No API key found.'
                };
            }

            const key = authorization.split(' ')[1];

            if (!key){
                return {
                    error: 'Invalid api key'
                };
            } 

            const keyResult = await this.pool.query('SELECT * FROM api_key WHERE key = $1 AND active = 1', [key]);

            if (keyResult.rowCount > 0) {
                return true;
            } else {
                return {
                    error: 'Invalid api key'
                };
            }
        }
        catch (e) {
            return {
                error: e.message
            }
        }
    }

    async generateKey(req, resp) {
        try {
            const apiKey = await crypto.randomBytes(32).toString('hex');
            const result = await this.pool.query('INSERT INTO api_key(key) VALUES($1) RETURNING ID', [apiKey]);
            
            resp.json({
                apiKey
            });
        }
        catch(e) {
            resp.json({
                error: e.message
            })
        }
    }
}

module.exports.Auth = Auth;